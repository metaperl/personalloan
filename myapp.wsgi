# -*- python -*-

# core
import os
import sys

# 3rd party
import cherrypy

# local
def full_path(*extra):
    return os.path.join(os.path.dirname(__file__), *extra)

output = u''
output += u'sys.version = %s\n' % repr(sys.version)
output += u'sys.prefix = %s\n' % repr(sys.prefix)
sys.stderr.write("PYTHON_VERSION: {}".format(output))

sys.path.insert(0, full_path())
sys.path.insert(0, '/home/schemelab/install/miniconda3/lib/python3.6/site-packages')

import config
import myapp



application = cherrypy.Application(myapp.Root(), "/", config.config)
