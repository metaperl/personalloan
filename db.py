"""Define the relational database schema.
"""

# Core
from datetime import datetime

# 3rd Party
from pydal import DAL, Field

#db = DAL('sqlite://storage.sqlite3')
db = DAL('sqlite:///home/schemelab/domains/com/iwantyoutoprosper/credit/storage.sqlite3')

market = db.define_table(
    'affiliate',
    Field('username', required=True),
    Field('name',     required=True),
    Field('phone',    required=True),
    Field('email',    required=True),
    Field('pic'),
    Field('skype'),
    Field('ts', type='datetime', default=datetime.now)
    )


db.executesql('CREATE INDEX IF NOT EXISTS aff_username ON affiliate (username);')
db.executesql('CREATE INDEX IF NOT EXISTS aff_ts       ON affiliate (ts);')


db.commit()
